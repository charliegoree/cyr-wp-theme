<?php get_header(); ?>
<div class="wrapper">
    <?php while ( have_posts() ) : the_post();?>
        <?php the_post_thumbnail(); ?>
        <div class="titulares">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="contenido">
            <?php the_content();?> 
        </div>
    <?php endwhile; ?>
</div>
<?php get_footer(); ?>