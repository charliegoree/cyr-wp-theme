<footer class="footer">
<p class=copyright >2019 powered by LUKE</p>
        <div class=contenedorFooter >
            <div class=contenedorFooter >
                <h1 class=tituloFooter >Navegacion</h1>
                <ul>
                    <li><a href="/#home" >Home</a></li>
                    <li><a href="/#quehacemos" >Que Hacemos</a></li>
                    <li><a href="/#blog" >Blog</a></li>
                    <li><a href="/#contacto" >Contacto</a></li>
                </ul>
            </div>
            <div class=contenedorFooter>
                <h1 class=tituloFooter>Encontranos en:</h1>
                <ul>
                    <li>
                        <i class="fas fa-map-marker-alt"></i>
                        <div class=doble>
                            <p>SHOWWROOM MAR DEL PLATA</p>
                            <p>Garay 1400 esq. Olavarria</p>
                        </div>
                    </li>
                    <li>
                        <i class="fas fa-map-marker-alt"></i>
                        <div class=doble>
                            <p>SHOWWROOM PINAMAR</p>
                            <p>Jupiter y Rivadavia</p>
                        </div>
                    </li>
                    <li>
                        <i class="fas fa-map-marker-alt"></i>
                        <div class=doble>
                            <p>SHOWWROOM VALERIA DEL MAR</p>
                            <p>Av. Espora 1590</p>
                        </div>
                    </li>
                </ul>
            </div>
            <img src="<?php echo get_stylesheet_directory_uri(). '/assets/logo.png' ?>" />
            <div class=contenedorFooter>
                <h1 class=tituloFooter>Redes Sociales:</h1>
                <ul class=redes>
                    <li><a href="https://www.instagram.com/cyramoblamientos/"> &nbsp;<i class="fab fa-instagram"></i></a></li>
                    <li><a href="https://www.facebook.com/amoblamientoscyr"> &nbsp;<i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="https://www.pinterest.es/cramoblamientos/"> &nbsp;<i class="fab fa-pinterest-p"></i></a></li>
                </ul>
            </div>
        </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>