<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha256-l85OmPOjvil/SOvVt3HnSSjzF1TUMyT9eV0c2BzEGzU=" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="<?php bloginfo('stylesheet_url'); ?>" rel = "stylesheet">
    <title>Backend C&R amoblamientos</title>
    <?php wp_head(); ?>
</head>
<body>
<header class="header">
<div class="menuBoton">
        <span> </span>
        <span> </span>
        <span> </span>
      </div>
      
      <div class="redes">
        <a href="https://www.instagram.com/cyramoblamientos/" class="item"><i class="fab fa-instagram"></i></a>
        <a href="https://www.facebook.com/amoblamientoscyr" class="item"><i class="fab fa-facebook-f"></i></a>
        <a href="https://www.pinterest.es/cramoblamientos/" class="item"><i class="fab fa-pinterest-p"></i></a>
        <div class="divisor"></div>
        <img src="<?php echo get_stylesheet_directory_uri(). '/assets/logo.png' ?>" />
      </div>
</header>